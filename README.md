# Introduction

GRINGOTTS allows you to store arbitrary data from R to your postgres database.

The configuration is done globally (per user) with the R options upon loading of the GRINGOTTS package.

The main idea is that you have one central storage of "stuff" data. You want to store arbitrary data with arbitrary data types from R occasionally or frequently by just typing one command. You do not need any predefined entities or relations (database design) and do not want to be dependent on noSQL forms of storage. 

# Use Case: Excel Connector

Whenever analysis in perfomed in R, there is a question how to export results. You can either choose to only provide end-users with graphics or export data in a machine readable format like in csv files or use some Excel libraries to export Excel files directly. All those options make updates in data and also distribution quite unflexible. Using a database sometimes (especially larger datasets) makes more sense. GRINGOTTS allows you to store those exported data into a general-purpose postgres DB that you can share with colleagues to make datasets available. 

# Use Case: ETL

In case you use R for ETL (extract, transform, load) purposes, you might need staging tables or temporary storages. Here, you can use GRINGOTTS to be fully generic with input data that you want to store. And the processing after GRINGOTTS can be performed by any tool that has access to the DB.

# Use Case: Analytics

Some results may take a long time to compute. For storing those results for later usage, you might want to use a database. However, you may not want to design a database schema first but rather store datasets as they come. GRINGOTTS will help you out and store anything you provide in a table. Results can be queried at a later stage or you could store the same computations and append to previous versions and look at the change over time.
